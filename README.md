How to build the application:
1. Open the folder Assignment#4
2. Right click and openwith Visual Studio Code
3. Open Terminal in VS Code.
4. Edit the files in public, view and layout folders accordingly
5. Save the changes
6. Open Terminal and Run the application

How to RUN the appication:
1. Open the folder Assignment#4
2. RIght click and openwith Visual Studio Code
3. Open Terminal in VS Code.
4. Run the command "npm install"
5. Once all the plugins are installed, run the application by executing the command "node index.js"
6. Open browser
7. go to the url "localhost:8080"

Why did I choose MIT License
MIT License is very easy to read and understand. We can reuse it, provided MIT terms and conditions along with the copyright notice. Also, It gives a clear idea about whoall can use this code without affecting the reputation of the publisher.